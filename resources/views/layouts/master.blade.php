<!-- Stored in resources/views/layouts/master.blade.php -->

<html>
<head>
    <title>App Name - @yield('title')</title>
</head>
<body>
@section('menubar')
    Home | Test | About us | Contact Us
@show

<div class="container">
    @yield('content')
</div>

<div class="footer">
    @yield('footer')
</div>
</body>
</html>