<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/bootstrap_resource/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/bootstrap_resource/css/bootstrap.min.css"><link rel="stylesheet" type="text/css" href="/bootstrap_resource/css/main.css">
</head>
<body>
	<div class="container">
		@yield('header')
	</div>

	<div class="container-fluid">
  		@yield('contant')
	</div>
	

	@yield('footer')

</body>
</html>