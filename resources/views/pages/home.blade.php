@extends('layout')

@section('title','Home')

@section('contant')

    <h1>This is Home test page </h1>

    My name is <?php echo $name; ?>, Id : <?php echo $id; ?>, Phone no. : <?php echo $phone; ?>, Title Name : <?php echo $title; ?>, and home town <?php echo $homeTown; ?><br>

    <h1>Use blade:</h1>
    My name is {{$name}}, Id: {{$id}}, Phone : {{$phone}}, Title : {{$title}}, Home Town: {{$homeTown}}

    <h3>if else : </h3>
    @if($id==963852741)
        Name : {{$name}}, Id : {{$id}}
    @else
        {{"Sorry your id not match"}}
    @endif
    <br><br>
    <h1>{{$arrayNew1["bd"]}}</h1> This output is array

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aperiam at consequatur eius facere illo illum nulla quo sit vero? Adipisci doloribus est nobis quas sapiente! Blanditiis deleniti facilis optio.</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Delectus distinctio eligendi excepturi facere facilis hic laboriosam magni maiores modi nulla omnis perspiciatis quam, quibusdam soluta voluptate. Deserunt est odio optio.</p>


@stop

@section('footer', 'home page')