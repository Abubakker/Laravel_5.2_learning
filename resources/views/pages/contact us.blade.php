@extends('layout')

@section('title','Contact Us')
    
@section('contant')

    <h1>
        without condition:
        @foreach($peopleName as $people)
            <li>{{$people}}</li>        
        @endforeach
        <br><br>

        simple condition:<br>
        @IF (empty($people))
            There are no people.
        @else
            Somthing else here
        @endif
        <br><br>

        condition and foreach:
        @foreach($peopleName as $people)
            @IF (empty($people))
                There are no people.
            @else
                <li>{{$people}}</li>
            @endif                
        @endforeach
        <br><br>
    </h1>
    @unless (empty($people))
        There are some people here.
    @endunless

@stop

@section('footer', 'Contact Us page')