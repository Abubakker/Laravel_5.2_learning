<!DOCTYPE html>
<html>
<head>
	<title>@yield('title')</title>
	<link rel="stylesheet" type="text/css" href="/bootstrap_resource/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/bootstrap_resource/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/bootstrap_resource/css/main.css">
</head>
<body>
	@include('pages.header')

	@yield('contant')

	
	@extends('pages.footer')
	@section('t_footer')
		@yield('footer')
	@stop
</body>
</html>