<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class test1Controller extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function index(){
        $name="Md. Abu Bakker Siddique ";
        $id="963852741 ";
        $phone="01798765432 ";
        $title="No Title ";
        $address="Chapainawabganj, Rajshahi";
        $arrayNew1=array("bd" => "Bangladesh", "us"=>"USA", "au"=>"Austry");
        return view('pages/home',compact('name','id','phone','title','arrayNew1'))->with('homeTown', $address);
    }
    public function about(){
    $myname="Md. Abu Bakker Siddique";
    return view('pages/about')->with('my_name', $myname);

    }


    public function contact(){
        $peopleName = array("abs" => "Md. Abu Bakker Siddique", "rio" =>"Md. Rayhan", "vola" => "Md. Asiqul Islam");
        return view('pages/contact us', compact('peopleName'));
    }


    public function home1(){
        $dis = array("cnpi" => array("abs" => "Md. Abu Bakker Siddique", "rio" =>"Md. Rayhan", "vola" => "Md. Asiqul Islam"), "npi" => "Nowga","cpi" => "Cox's bazar");
        return $dis;
        return view('home1', compact('dis'));
    }

}
   // <?php
   //       $marks = array( 
   //          "mohammad" => array (
   //             "physics" => 35,
   //             "maths" => 30,   
   //             "chemistry" => 39
   //          ),
            
   //          "qadir" => array (
   //             "physics" => 30,
   //             "maths" => 32,
   //             "chemistry" => 29
   //          ),
            
   //          "zara" => array (
   //             "physics" => 31,
   //             "maths" => 22,
   //             "chemistry" => 39
   //          )
   //       );
         
   //       /* Accessing multi-dimensional array values */
   //       echo "Marks for mohammad in physics : " ;
   //       echo $marks['mohammad']['physics'] . "<br />"; 
         
   //       echo "Marks for qadir in maths : ";
   //       echo $marks['qadir']['maths'] . "<br />"; 
         
   //       echo "Marks for zara in chemistry : " ;
   //       echo $marks['zara']['chemistry'] . "<br />"; 
   //    ?>
   // 