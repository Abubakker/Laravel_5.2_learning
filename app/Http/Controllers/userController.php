<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

class userController extends Controller
{
    public function index_user(){
        return view('users.index_user');
    }

    public function sign_up(){
        return view('users.sign_up');
    }

    public function login(){
        return view('users.login');
    }

    public function login_view(){
        return view('users.login_view');
    }

    public function store(Request $request)
    {
        // return $request->all();
        $user = new User();
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        if($request->input('password') == $request->input('password_confirmation')){
            $user->save();
            return redirect(action('userController@login'));
        }
        return redirect()->back();

        
    }

    public function user_login(Request $request)
    {
          // return $request->all();
        $email = $request->input('email');
        // $password = $request->input('password'));

        if (Auth::attempt(['email'=>$email, 'password'=>$password])){
            return redirect(action('userController@login_view'));
        }
        return redirect()->back();
    }

    public function logout(){
        Auth::logout();
        return redirect('userController@index_user');
    }
















    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
