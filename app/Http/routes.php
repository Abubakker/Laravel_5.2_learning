<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

 // For user

Route::get('index_user', 'userController@index_user');
Route::get('sign_up', 'userController@sign_up');
Route::get('login', 'userController@login');
Route::get('login_view', 'userController@login_view');
Route::post('store','userController@store');
Route::get('user_login', 'userController@user_login');


// For user 




Route::resource('create', 'crudController');

Route::get('/', function () {
    return view('welcome');
});

Route::get('test', function () {
    return "Are you happy !";
});

Route::get('contact', function (){
    return view('contact');
});

Route::get('all user', function(){
    return view ('all user');
});

Route::get('profile', 'MyController@index');
Route::get('test_home', 'MyCountroller@index');
Route::get('about', 'MyController@about');
Route::get('new_abc', 'MyControllerMyController@abc');

//after eid

Route::get('home', 'test1Controller@index');
Route::get('about', 'test1Controller@about');
Route::get('contact us', 'test1Controller@contact');

Route::get('home1', 'test1Controller@home1');

Route::get('test', function(){
	return view('pages/test');
});

Route::get('alert', function(){
	return view('pages/alert');
});